# puppet-control
This Control repository is part of the course setting up your manageable puppet infrastructure on Udemy. With the link below you can buy it at a respectable discount.

If you want to use this repository, please purchase the course as well.

# Course URL
If you buy the course with this URL you will get a huge discount and get the course for only €19.99

https://www.udemy.com/setting-up-your-manageable-puppet-infrastructure/?couponCode=GITLABREPO

# Summary
In this course we will build a fully functioning Manageable Puppet Infrastructure, fully deployed from puppet code.

This infrastructure has been deployed at many companies, and works very well in both very large and small environments.

It is updated with the latest best practices and works with Roles and Profiles, r10k deployments, Hiera eyaml et cetera. Puppetboard is used as the GUI.

This course delivers a huge amount of value, but is not suitable for the beginner. A firm grasp of puppet and module structure is a prerequisite for this course. If you are a beginner I can recommend the Learning Puppet VM that is provided by Puppet

# Commands to deploy server

rpm -Uvh https://yum.puppet.com/puppet5/puppet5-release-el-7.noarch.rpm

yum -y install puppet-agent git puppet-bolt

ssh-keygen

cat /root/.ssh/id_rsa.pub # use this for Gitlab.com: Settings->Repository->Deploy Keys

mkdir temp

cd temp

git clone git@gitlab.com:ger-udemy/puppet-control.git

cd puppet-control/

/opt/puppetlabs/bolt/bin/r10k puppetfile install -v

FACTER_role=puppetmaster puppet apply manifests/site.pp --modulepath=./site:./modules --hiera_config=./hiera.yaml

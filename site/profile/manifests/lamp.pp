# A description of what this class does
#
# @summary A short summary of the purpose of this class
#
# @example
#   include profile::lamp
class profile::lamp {
  include apache
  include apache::mod::php
  include mysql::server

  create_resources('apache::vhost', lookup('apache::vhost', Hash, 'hash', {}))
  create_resources('mysql::db', lookup('mysql::db', Hash, 'hash', {}))
}

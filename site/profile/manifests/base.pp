# A description of what this class does
#
# @summary A short summary of the purpose of this class
#
# @example
#   include profile::base
class profile::base {
  include ntp

  file { ['/etc/puppetlabs/facter', '/etc/puppetlabs/facter/facts.d']:
    ensure => directory,
  }
  create_resources('file', lookup('file', Hash, 'hash', {}))
  create_resources('firewall', lookup('firewall', Hash, 'hash', {}))
  create_resources('augeas', lookup('augeas', Hash, 'hash', {}))
}

# A description of what this class does
#
# @summary A short summary of the purpose of this class
#
# @example
#   include profile::puppetboard
class profile::puppetboard {
  # Configure Apache on this server
  include apache
  include apache::mod::wsgi

  # Configure Puppetboard
  include puppetboard
  include puppetboard::apache::vhost
}

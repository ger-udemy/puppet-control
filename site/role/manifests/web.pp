# A description of what this class does
#
# @summary A short summary of the purpose of this class
#
# @example
#   include role::web
class role::web {
  include profile::base
  include profile::lamp
}

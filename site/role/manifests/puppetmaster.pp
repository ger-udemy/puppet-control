# A description of what this class does
#
# @summary A short summary of the purpose of this class
#
# @example
#   include role::puppetmaster
class role::puppetmaster {
  include profile::base
  include profile::puppetmaster
  include profile::puppetboard
}
